﻿using System;

namespace Game2048
{
	internal class GameRenderer
	{
		private readonly Game _game;

		public GameRenderer(Game game)
		{
			_game = game;
		}

		public void Display()
		{
			Console.Clear();
			Console.WriteLine();
			var nRows = _game.GameBoard.nRows;
			var nCols = _game.GameBoard.nCols;

			for (var i = 0; i < nRows; i++)
			{
				for (var j = 0; j < nCols; j++)
				{
					var currentNum = _game.GameBoard.Board[i, j];
					using (new ColorOutput(GetNumberColor(currentNum)))
					{
						Console.Write($"{currentNum,6}");
					}
				}

				Console.WriteLine();
				Console.WriteLine();
			}

			Console.WriteLine("Score: {0}", _game.Score);
			Console.WriteLine();
		}

		private static ConsoleColor GetNumberColor(ulong num)
		{
			switch (num)
			{
				case 0:
					return ConsoleColor.DarkGray;

				case 2:
					return ConsoleColor.Cyan;

				case 4:
					return ConsoleColor.Magenta;

				case 8:
					return ConsoleColor.Red;

				case 16:
					return ConsoleColor.Green;

				case 32:
					return ConsoleColor.Yellow;

				case 64:
					return ConsoleColor.Yellow;

				case 128:
					return ConsoleColor.DarkCyan;

				case 256:
					return ConsoleColor.Cyan;

				case 512:
					return ConsoleColor.DarkMagenta;

				case 1024:
					return ConsoleColor.Magenta;

				default:
					return ConsoleColor.Red;
			}
		}

		public void PrintDeadMessage()
		{
			using (new ColorOutput(ConsoleColor.Red))
			{
				Console.WriteLine("YOU ARE DEAD!!!");
			}
		}

		private class ColorOutput : IDisposable
		{
			public ColorOutput(ConsoleColor fg, ConsoleColor bg = ConsoleColor.Black)
			{
				Console.ForegroundColor = fg;
				Console.BackgroundColor = bg;
			}

			public void Dispose()
			{
				Console.ResetColor();
			}
		}
	}
}