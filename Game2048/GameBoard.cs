﻿using System;
using System.Collections.Generic;

namespace Game2048
{
	internal class GameBoard
	{
		private GameBoard(ulong[,] board)
		{
			Board = (ulong[,])board.Clone();
		}

		public GameBoard(int rowSize, int columnSize)
		{
			Board = new ulong[rowSize, columnSize];
		}

		public ulong[,] Board { get; }
		public int nRows => Board.GetLength(0);
		public int nCols => Board.GetLength(1);

		public GameBoard Clone()
		{
			var gameBoard = new GameBoard(Board);
			return gameBoard;
		}


		public bool Update(Direction direction, out ulong score)
		{
			score = 0;
			var hasUpdated = false;

			// You shouldn't be dead at this point. We always check if you're dead at the end of the Update()
			var loopingParameters = new LoopingParameters(direction, nRows, nCols);


			for (var i = 0; i < loopingParameters.OuterCount; i++)
				for (var j = loopingParameters.InnerStart; innerCondition(j, loopingParameters); j = ReverseDrop(j, loopingParameters.IsIncreasing))
				{
					if (GetValue(i, j, loopingParameters.IsAlongRow) == 0) continue;

					var newJ = j;
					do
					{
						newJ = Drop(newJ, loopingParameters.IsIncreasing);
					}


					// Continue probing along as long as we haven't hit the boundary and the new position isn't occupied
					while (innerCondition(newJ, loopingParameters) && GetValue(i, newJ, loopingParameters.IsAlongRow) == 0);

					if (innerCondition(newJ, loopingParameters) && GetValue(i, newJ, loopingParameters.IsAlongRow) == GetValue(i, j, loopingParameters.IsAlongRow))
					{
						// We did not hit the canvas boundary (we hit a node) AND no previous merge occurred AND the nodes' values are the same
						// Let's merge
						var newValue = GetValue(i, newJ, loopingParameters.IsAlongRow) * 2;
						SetValue(i, newJ, newValue, loopingParameters.IsAlongRow);
						SetValue(i, j, 0, loopingParameters.IsAlongRow);

						hasUpdated = true;
						score += newValue;
					}
					else
					{
						// Reached the boundary OR...
						// we hit a node with different value OR...
						// we hit a node with same value BUT a previous merge had occurred
						//
						// Simply stack along
						newJ = ReverseDrop(newJ, loopingParameters.IsIncreasing); // reverse back to its valid position
						hasUpdated = newJ != j;

						var value = GetValue(i, j, loopingParameters.IsAlongRow);
						SetValue(i, j, 0, loopingParameters.IsAlongRow);
						SetValue(i, newJ, value, loopingParameters.IsAlongRow);
					}
				}

			return hasUpdated;
		}

		private bool innerCondition(int index, LoopingParameters loopingParameters)
		{
			return Math.Min(loopingParameters.InnerStart, loopingParameters.InnerEnd) <= index
				   && index <= Math.Max(loopingParameters.InnerStart, loopingParameters.InnerEnd);
		}

		private int Drop(int innerIndex, bool isIncreasing)
		{
			return innerIndex + (isIncreasing ? -1 : 1);
		}

		private int ReverseDrop(int innerIndex, bool isIncreasing)
		{
			return innerIndex + (isIncreasing ? 1 : -1);
		}

		private ulong GetValue(int i, int j, bool isAlongRow)
		{
			return isAlongRow ? Board[i, j] : Board[j, i];
		}

		private void SetValue(int rowIndex, int columnIndex, ulong newValue, bool isAlongRow)
		{
			int index1, index2;
			if (isAlongRow)
			{
				index1 = rowIndex;
				index2 = columnIndex;
			}
			else
			{
				index1 = columnIndex;
				index2 = rowIndex;
			}

			Board[index1, index2] = newValue;
		}

		private List<Tuple<int, int>> FindEmptySlots()
		{
			var emptySlots = new List<Tuple<int, int>>();
			for (var iRow = 0; iRow < nRows; iRow++)
				for (var iCol = 0; iCol < nCols; iCol++)
					if (Board[iRow, iCol] == 0)
						emptySlots.Add(new Tuple<int, int>(iRow, iCol));

			return emptySlots;
		}

		public void PutNewValue(Random random)
		{
			// Find all empty slots
			var emptySlots = FindEmptySlots();

			// We should have at least 1 empty slot. Since we know the user is not dead
			var iSlot = random.Next(0, emptySlots.Count); // randomly pick an empty slot
			var value = random.Next(0, 100) < 95
				? 2UL
				: 4UL; // randomly pick 2 (with 95% chance) or 4 (rest of the chance)
			Board[emptySlots[iSlot].Item1, emptySlots[iSlot].Item2] = value;
		}
	}

	public class LoopingParameters
	{
		public bool IsAlongRow { get; }
		public bool IsIncreasing { get; }

		public int OuterCount => IsAlongRow ? nRows : nCols;
		public int InnerCount => IsAlongRow ? nCols : nRows;
		public int InnerStart => IsIncreasing ? 0 : InnerCount - 1;
		public int InnerEnd => IsIncreasing ? InnerCount - 1 : 0;

		private readonly int nRows;
		private readonly int nCols;

		public LoopingParameters(Direction direction, int nRows, int nCols)
		{
			IsAlongRow = direction == Direction.Left || direction == Direction.Right;
			IsIncreasing = direction == Direction.Left || direction == Direction.Up;

			this.nRows = nRows;
			this.nCols = nCols;
		}
	}
}