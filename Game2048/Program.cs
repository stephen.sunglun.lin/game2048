﻿namespace Game2048
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var game = new Game();
			game.Run();
		}
	}
}